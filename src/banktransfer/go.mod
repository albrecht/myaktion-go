module gitlab.reutlingen-university.de/albrecht/myaktion-go/src/banktransfer

go 1.20

require (
	github.com/golang/protobuf v1.5.3
	github.com/segmentio/kafka-go v0.4.40
	github.com/sirupsen/logrus v1.9.0
	google.golang.org/grpc v1.55.0
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
)
