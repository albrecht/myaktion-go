# Add Campaign
curl -H "Content-Type: application/json" -d '{"name":"Covid","organizerName":"Martin","donationMinimum":2,"targetAmount":100,"account":{"name":"Martin","bankName":"DKB","number":"123456"}}' localhost:8000/campaigns


# Get (all) campaigns
curl localhost:8000/campaigns

# Get (specified) campaign
curl localhost:8000/campaigns/2

# DeleteCampaign
curl -X DELETE localhost:8000/campaigns/1

# UpdateCampaign
curl -X PUT -H "Content-Type: application/json" -d '{"name":"new Name","organizerName":"Thomas","donationMinimum":2,"targetAmount":100,"account":{"name":"Martin","bankName":"DKB","number":"123456"}}' localhost:8000/campaigns/1 

# AddDonation
curl -H "Content-Type: application/json" -d '{"CampaignID":1,"amount":12.23,"DonorName":"Stefan","ReceiptRequested":false,"Status":"TRANSFERRED"}' localhost:8000/campaigns/1/donation